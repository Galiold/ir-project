from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from bs4 import BeautifulSoup

# TODO: Find a way to limit webpages retrieved from each webpage
# https://stackoverrun.com/fr/q/9496481

class IMDBSpider(CrawlSpider):
    name = 'Collider'
    allowed_domains = ['collider.com']
    denied_domains = ['']
    start_urls = [
        'https://collider.com/'
    ]

    rules = [
        Rule(
            LinkExtractor(canonicalize=True, unique=True, allow_domains=allowed_domains),
            callback='parse_page',
            follow=True
            )
    ]


    def parse_page(self, response):
        url = response.request.url
        denied_sources = [
            'collider.com/tag',
        ]
        # if 'imdb.com/title' in url or 'imdb.com/name' in url:
        for src in denied_sources:
            if src not in url:
                yield {
                    'link': response.request.url,
                    'title': response.css('title::text').get(),
                    'text': BeautifulSoup(response.body).get_text().strip().replace('\n', '')
                }
                break
