from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from bs4 import BeautifulSoup

# TODO: Find a way to limit webpages retrieved from each webpage
# https://stackoverrun.com/fr/q/9496481

class RottenSpider(CrawlSpider):
    name = 'Rotten'
    allowed_domains = ['rottentomatoes.com']
    denied_domains = ['editorial.rottentomatoes.com']
    start_urls = [
        'https://www.rottentomatoes.com/'
    ]

    rules = [
        Rule(
            LinkExtractor(canonicalize=True, unique=True, allow_domains=allowed_domains, deny_domains=denied_domains),
            callback='parse_page',
            follow=True
            )
    ]

    def parse_page(self, response):
        url: str = response.request.url
        if 'rottentomatoes.com/tv' in url or 'rottentomatoes.com/m' in url or 'rottentomatoes.com/celebrity' in url:
            yield {
                'link': response.request.url,
                'title': response.css('title::text').get(),
                'text': BeautifulSoup(response.body).get_text().strip().replace('\n', '')
            }
