from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from bs4 import BeautifulSoup

# TODO: Find a way to limit webpages retrieved from each webpage
# https://stackoverrun.com/fr/q/9496481

class IMDBSpider(CrawlSpider):
    name = 'IMDB'
    allowed_domains = ['imdb.com']
    denied_domains = ['pro.imdb.com']
    start_urls = [
        'https://www.imdb.com/'
    ]

    rules = [
        Rule(
            LinkExtractor(canonicalize=True, unique=True, allow_domains=allowed_domains, deny=['imdb.com/ap/*'], deny_domains=denied_domains),
            callback='parse_page',
            follow=True
            )
    ]

    def parse_page(self, response):
        url = response.request.url
        if 'imdb.com/title' in url or 'imdb.com/name' in url:
            yield {
                'link': response.request.url,
                'title': response.css('title::text').get(),
                'text': BeautifulSoup(response.body).get_text().strip()
            }
